﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace nhdnotify
{

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //this.Visible = false;
            this.Hide();
            this.ShowInTaskbar = false;
            this.WindowState = FormWindowState.Minimized;
            try
            {
                StreamReader sr = new StreamReader("config.cfg");
                textBox1.Text = sr.ReadLine();
                textBox2.Text = sr.ReadLine();
                textBox3.Text = sr.ReadLine();

                checkBox1.Checked = sr.ReadLine() == "True";
                checkfree.Checked = sr.ReadLine() == "True";
                checkpin.Checked = sr.ReadLine() == "True";

                int len = 0;
                len =Convert.ToInt32(sr.ReadLine());
                
                for (int i = 0; i < len; i++)
                {
                    textBox4.Text = textBox4.Text + sr.ReadLine();
                    if (i!=len-1)
                        textBox4.Text=textBox4.Text+"\r\n";
                }
                timer1.Enabled = (sr.ReadLine() == "True");
                buttonstop.Visible = timer1.Enabled;
                buttonstart.Visible = !timer1.Enabled;
                sr.Close();
            }
            catch (Exception ex) { 
            
            }
        }

        private void Form1_Click(object sender, EventArgs e)
        {



        }
        string nowshowid;
        private void show(string title, string data, string id) {
            listBox1.Items.Add(title + " " + data);
            nowshowid = id;
            notifyIcon1.BalloonTipTitle = title;
            notifyIcon1.BalloonTipText = data;
            notifyIcon1.ShowBalloonTip(200000);
        }
        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            this.Show();
            this.Activate();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.notifyIcon1.Visible = false;
            Application.Exit();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }
        private string get(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "get";
                CookieContainer cc = new CookieContainer();
                request.CookieContainer = cc;
//www.nexushd.org	FALSE	/	FALSE	2147483647	c_secure_uid	MzQ0Mzg%3D
//www.nexushd.org	FALSE	/	FALSE	2147483647	c_secure_pass	470dbc694829103ae3b74ac02350c6b7
//www.nexushd.org	FALSE	/	FALSE	2147483647	c_secure_ssl	bm9wZQ%3D%3D
//www.nexushd.org	FALSE	/	FALSE	2147483647	c_secure_tracker_ssl	bm9wZQ%3D%3D
//www.nexushd.org	FALSE	/	FALSE	2147483647	c_secure_login	bm9wZQ%3D%3D
                request.CookieContainer.Add(new Cookie("c_secure_uid", "MzQ1NzY%3D", "/", "www.nexushd.org"));
                request.CookieContainer.Add(new Cookie("c_secure_pass", "88b1f138804c162dd4c40463f10a7370", "/", "www.nexushd.org"));
                request.CookieContainer.Add(new Cookie("c_secure_ssl", "bm9wZQ%3D%3D", "/", "www.nexushd.org"));
                request.CookieContainer.Add(new Cookie("c_secure_tracker_ssl", "bm9wZQ%3D%3D", "/", "www.nexushd.org"));
                request.CookieContainer.Add(new Cookie("c_secure_login", "bm9wZQ%3D%3D", "/", "www.nexushd.org"));
                request.Timeout = 10000;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream resStream = response.GetResponseStream();
                string tempString = null;
                int count = 0;
                byte[] buf = new byte[8192];
                do
                {
                    // fill the buffer with data
                    count = resStream.Read(buf, 0, buf.Length);

                    // make sure we read some data
                    if (count != 0)
                    {

                        tempString += Encoding.UTF8.GetString(buf, 0, count);


                        // continue building the string

                    }
                } while (count > 0); // any more data to read?
                int len = tempString.Length;
                return tempString;
            }
            catch (Exception e) {
                //string gao = e.ToString();
                return "";
            }
        }
        private Seed[] list= new Seed[200];
        private Seed[] oldlist = new Seed[200];
        private int lennow=0;
        private int lenold = 0;
        private void parse(string data) {
            string start = "class=\"torrents\"";
            int t=data.IndexOf(start);
            t += 100;
            lennow = 0;
            do
            {
                int s = data.IndexOf("<tr", t);
                if (s == -1)
                    break;
                int now = 1;
                for (int j = s + 1; true; j++)
                {
                    if (data[j] == '<' && data[j + 1] == 't' && data[j + 2] == 'r')
                        now++;
                    if (data[j] == '<' && data[j + 1] == '/' && data[j + 2] == 't' && data[j + 3] == 'r')
                        now--;
                    if (now == 0)
                    {
                        t = j;
                        break;
                    }
                }
                list[lennow++]=new Seed(data.Substring(s,t-s+1));
            } while (true);
            
        }
        bool first = true;
        bool netdownalert = true;
        bool netupalert = true;
        int fileid = 0;
        private void download(string id) {
            fileid++;
            string url = "http://www.nexushd.org/download.php?id=" + id;
            try
            {
                {
                    System.Diagnostics.Process pProcess = new System.Diagnostics.Process();
                    pProcess.StartInfo.FileName = "wget";
                    pProcess.StartInfo.Arguments = "--load-cookies=c:\\windows\\cookies.txt http://www.nexushd.org/download.php?id="+id+" -O "+textBox1.Text+"/nhd"+fileid+".torrent";
                    pProcess.StartInfo.RedirectStandardOutput = true;
                    pProcess.StartInfo.CreateNoWindow = true;
                    pProcess.StartInfo.UseShellExecute = false;
                    pProcess.Start();
                    pProcess.WaitForExit(5000);
                }
                {
                    System.Diagnostics.Process pProcess = new System.Diagnostics.Process();
                    pProcess.StartInfo.FileName = textBox3.Text;
                    pProcess.StartInfo.Arguments = "/DIRECTORY " + textBox2.Text + " "+textBox1.Text + "/nhd" + fileid + ".torrent";
                    pProcess.StartInfo.RedirectStandardOutput = true;
                    pProcess.StartInfo.CreateNoWindow = true;
                    pProcess.StartInfo.UseShellExecute = false;
                    pProcess.Start();
                    show("Automatic download started", "Stored in "+textBox2.Text, "");
                }
            }
            catch (Exception e)
            {
                show("Failed to get the torrent file."," ","");
                return ;
            }
        }
        bool fittered(string title) {
            for (int i = 0; i < textBox4.Lines.Length; i++) {
                if (title.Contains(textBox4.Lines.ElementAt(i)))
                    return true;
            }
            return false;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            string url = "http://www.nexushd.org/torrents.php";
//            string url = "http://localhost/tmp/torrent.php";
            string data = get(url);
            if (data == "")
            {
                netupalert = true;
                this.notifyIcon1.Text = "Nexushd offline";
                if (netdownalert)
                {
                    show("Cannot access to nexushd", "network may be down", "");
                    netdownalert = false;
                }
                return;
            }
            else
            {
                netdownalert = true;
                this.notifyIcon1.Text = "Nexushd online";
                if (netupalert) {
                    show("Connection established", " ", "");
                    netupalert = false;                
                }
            }
            for (int i=0;i<lennow;i++)
                oldlist[i]=list[i];
            lenold=lennow;
            parse(data);
            
            for (int i = 0; i < lennow; i++) { 
                bool appeared=false;
                for (int j = 0; j < lenold; j++)
                    if (list[i].id == oldlist[j].id && list[i].tag==oldlist[j].tag && list[i].pin ==oldlist[j].pin)
                        appeared = true;
                if (!appeared && !first) {
                    if ((checkfree.Checked && list[i].tag.Contains("free")) || (list[i].pin && checkpin.Checked) || (fittered(list[i].title)))
                    {
                        show("New Important " + list[i].type + " Torrent", list[i].title + "\n" + list[i].description, list[i].id);
                        if (this.checkBox1.Checked)
                            download(list[i].id);
                    }
                    else
                    {
                        show("New " + list[i].type + " Torrent", list[i].title + "\n" + list[i].description, list[i].id);
                    }
                }
                
            }
            first=false;
        }

        private void notifyIcon1_BalloonTipClicked(object sender, EventArgs e)
        {
            string id = nowshowid;
            if (id!="")
                System.Diagnostics.Process.Start("http://www.nexushd.org/details.php?id="+id+"&hit=1");
            

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                this.Hide();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buttonstop.Visible = true;
            buttonstart.Visible = false;
            timer1.Enabled = true;
        }

        private void buttonstop_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            buttonstop.Visible = false;
            buttonstart.Visible = true;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void buttonsave_Click(object sender, EventArgs e)
        {
            
            try {
                StreamWriter sw=new StreamWriter("config.cfg");
                sw.WriteLine(textBox1.Text);
                sw.WriteLine(textBox2.Text);
                sw.WriteLine(textBox3.Text);
                sw.WriteLine(checkBox1.Checked);
                sw.WriteLine(checkfree.Checked);
                sw.WriteLine(checkpin.Checked);
                sw.WriteLine(textBox4.Lines.Length);
                for (int i = 0; i < textBox4.Lines.Length; i++) {
                    sw.WriteLine(textBox4.Lines[i]);
                }
                sw.WriteLine(timer1.Enabled);
                sw.Close();
                
            } catch (Exception ex) {
            
            }


        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            this.Show();
            this.Activate();
        }

        protected static float CalculateFolderSize(string folder)
        {
            float folderSize = 0.0f;
            try
            {
                //Checks if the path is valid or not
                if (!Directory.Exists(folder))
                    return folderSize;
                else
                {
                    try
                    {
                        foreach (string file in Directory.GetFiles(folder))
                        {
                            if (File.Exists(file))
                            {
                                FileInfo finfo = new FileInfo(file);
                                folderSize += finfo.Length;
                            }
                        }

                        foreach (string dir in Directory.GetDirectories(folder))
                            folderSize += CalculateFolderSize(dir);
                    }
                    catch (NotSupportedException e)
                    {
                        Console.WriteLine("Unable to calculate folder size: {0}", e.Message);
                    }
                }
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine("Unable to calculate folder size: {0}", e.Message);
            }
            return folderSize;
        }
    }
    public class Seed
    {
        public string type;
        public string title;
        public string description;
        public double size;
        public int uploader;
        public int downloader;
        public int completed;
        public string id;
        public string publisher;
        public string tag;
        public bool pin;
        private string parser(string data, string beg, string end) {
            int si = data.IndexOf(beg);
            if (si == -1)
                return "";
            si += beg.Length;
            int ei = data.IndexOf(end, si + 1);
            if (ei == -1)
                return "";
            return data.Substring(si, ei - si);
            
        }
        public Seed(String data)
        {
            this.pin = data.Contains("<img class=\"sticky\" src=\"pic/trans.gif\"");
            this.title = parser(data,"<a title=\"","\"");
            this.id = parser(data, "?id=", "&");
            this.tag = parser(data, "<tr class=\'", "\'");
            this.type = parser(data, ".gif\" alt=\"", "\"");
            this.description = parser(data, "<br />", "<");
            if (this.description.Contains("javascript"))
                this.description = "";

        }
    }
}
